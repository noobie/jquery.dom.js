
var _console = window.console

console = {}
body = $('body')

console.log = function(){
    _console.log.apply(_console, arguments)
}
console.warn = function(){
    _console.warn.apply(_console, arguments)
}
console.info = function(){
    _console.info.apply(_console, arguments)
}
console.debug = function(){
    _console.debug.apply(_console, arguments)
}

console.error = function(){
    _console.error.apply(_console, arguments)
}

console.info('starting jquery.dom test suite')

var prop, f, elem

$(function(){
    var val
    var shortcuts = $.dom._shortcuts
    for(var i=0; i < shortcuts.length; i++){
        prop = shortcuts[i]
        f = $.dom[prop]
        val = 'foo'
        if(prop == 'script'){
            val = '"foo"'
        }
        elem = f(null, val)
        console.info('appending elem', elem, elem[0])
        body.append(elem)
    }
})