/* dom.jquery.js
 *
 * This module is intended to make the generation of jQuery-wrapped DOM
 * elements simpler and using the functional paradigm of programming.
 * Similar design to what MochiKit.js but modified for the jQuery framework.
 */

(function($){
    var partial, i, prop, shortcuts
    // curry-style sugar so we can do partials without another library
    partial = function(func/*, args, ...*/){
        var args_ = arguments
        return function(){
            var args, i

            args = []
            // push our paritally applied args
            for(i = 1; i < args_.length; i++){
                args.push(args_[i])
            }
            // push our remaining args
            for(i = 0; i < arguments.length; i++){
                args.push(arguments[i])
            }
            return func.apply(null, args)
        }
    }
    
    $.dom = {}
    
    $.dom.flatten_non_jquery = function flatten_non_jquery(obj){
        if(typeof obj.jquery === 'string'){
            return [obj]
        }
        var merged = []
        return merged.concat.apply(merged, obj)
    }

    $.dom.elem = function(type, attrs/*, children, ... */){
        var elem, $elem, children, i;

        // create our element, <div/> by default
        type = (type) ? type : 'div'
        elem = document.createElement(type)
        $elem = $(elem)
        children = []
        attrs = (attrs) ? attrs : {}

        children = Array.prototype.slice.call(arguments, 2)
        children = $.dom.flatten_non_jquery(children)

        // copy all the properties to the element
        for(i in attrs){
            $elem.attr(i, attrs[i])
        }

        // append all our children, since we allow strings we need  to do it
        // piece-wise because jQuery doesn't like string children... (deadbeat)
        children.forEach(function(child, index, arr){
            if(typeof child === 'string'){
                return $elem.html([$elem.html(), child].join(''))
            }
            $(child).appendTo($elem)
        })

        return $elem
    }

    // shortcuts from https://developer.mozilla.org/en-US/docs/Web/HTML/Element
    shortcuts = ["elementname","title","a","abbr","acronym","address","applet","area","article","aside","audio","b","base","basefont","bdi","bdo","bgsound","big","blink","blockquote","body","br","button","canvas","caption","center","cite","code","col","colgroup","content","data","datalist","dd","decorator","del","details","dfn","dialog","dir","div","dl","dt","element","em","embed","fieldset","figcaption","figure","font","footer","form","frame","frameset","h1","h2","h3","h4","h5","h6","head","header","hgroup","hr","html","i","iframe","img","input","ins","isindex","kbd","keygen","label","legend","li","link","listing","main","map","mark","marquee","menu","menuitem","meta","meter","nav","nobr","noframes","noscript","object","ol","optgroup","option","output","p","param","picture","plaintext","pre","progress","q","rp","rt","ruby","s","samp","script","section","select","shadow","small","source","spacer","span","strike","strong","style","sub","summary","sup","table","tbody","td","template","textarea","tfoot","th","thead","time","title","tr","track","tt","u","ul","var","video","wbr","xmp"]
    $.dom._shortcuts = shortcuts
    
    for(var n=0; n < shortcuts.length; n++){
        prop = shortcuts[n]
        $.dom[prop] = partial($.dom.elem, prop)
        
        // for backwards compat
        if(!$[prop]){
            $[prop] = partial(function(prop){
                if(!$[prop].__warned){
                    console.warn('use of the $.' + prop + '() is now deprecated, please use the $.d.' + prop + '() via the $.d namespace.')
                    $[prop].__warned = 1
                }
                var args = Array.prototype.slice.apply(arguments, [0])
                return $.dom.elem.apply(null, args)
            }, prop)
        }
    }
    
    $.dom.nbsp = '&nbsp;'
    
    // backwards compat
    $.elem = $.dom.elem
    
    // shortcut
    $.d = $.dom
}(jQuery))
