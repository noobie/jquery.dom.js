# jquery.dom.js
--------------

Ten seconds to learn how to use the plugin:

```javascript
// your first element
var jquery_wrapped_element = $.h1({'class':'cssClass', 'id':'uniqueId'})

// your second element, and it's nested
var second_element = $.div({'class':'jumbo'}, jquery_wrapped_element)

// nested elements with strings too
var page_blob = $.elem('section'
					 , 'this is my section'
					 , second_element
					 , 'it has things and stuff'
					 , $.a({'href':'http://txtxt.trioson.com'}, 'last minute')
					 )
					 
```

## You want the details? I've got the details...

Remember, you can make any HTMLElement that the browser DOM will accept with:
```javascript
// the base of all elements
$.elem('a'           // type of dom element
	  ,{'foo':'bar'} // attributes for the node
	  ,/* children */
)
```

You can customize any element's attributes with:
```javascript
// a specific element
$.a({'href':'http://www.rainymood.com'}))
```

Here are some examples:
```javascript
// a non-regular dom type
$.elem('section', {'class':'cheap-section'})

// a regular link
$.a({'href':'#/freedom'}, 'Free At Last')

// a nest
$.div({'class':'buy-me'}, $.a({'href':'#/buy-now'}))
```


## What are the shortcuts available now?

As of first release we have:
```javascript

$.div()
$.input()
$.button()
$.span()
$.p()
$.br()
$.img()
$.i()
$.sub()
$.sup()
$.a()

// not a function, although debatably should be
$.nbsp

```

## Your missing my favorite element type as a shortcut, daw :(
Just fork and request, dude! Anyone's input is welcome.


This was adapted from MochiKit.js but modified for the jQuery framework,
thereby removing nasty capital letter programming.